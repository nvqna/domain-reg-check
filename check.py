#!/usr/bin/python3

import re
import time
import tldextract
import whois #pip3 install python-whois
from sys import argv
from prettytable import PrettyTable

# takes a file and returns a list of possible tlds, based on a regex
def runregex(infile):
    print("Searching for TLDs in", infile)
    # check the host and referer headers. many of these wont be caught by the main regex
    host_referer_regex = r'Host|Referer: (.*)' #results in group 1
    url_regex = r'\w+://\w+\.*\w+\.\w+/?[\w\.\?=#]*'

    with open(infile) as f:
        contents = f.read()
        host_matches = re.findall(host_referer_regex, contents)
        anything_else_matches = re.findall(url_regex, contents)
        # join all the lists together
        all_to_check = host_matches + anything_else_matches
        cleaned_to_check = clean_urls(all_to_check)
    return cleaned_to_check


# takes a list of strings and checks if they are valid tlds.
def validate_tlds(possible_tlds):
    tlds = []
    for item in set(possible_tlds):
        tld = tldextract.extract(item).registered_domain
        if tld:
            tlds.append(tld)

    return tlds



def clean_urls(url_list):
    clean_urls = []
    for url in url_list:
        url = url.replace('https://', '')
        url = url.replace('http://', '')
        # also remove anything after the first remaining /
        url = url.split("/")[0]
        url = url.split("#")[0]
        url = url.split("?")[0]
        url = url.split("=")[0]
        clean_urls.append(url)
    return clean_urls

# this takes a list of tlds and returns a list containing two dicts:
#  whois_data_registered and whois_data_unregistered
#  each of these dicts contain a list of dicts, with each dict representing the
#  results of a whois.
def whois_lookup(tlds):
    sleep_time = 15
    print("TLDs identified:", ', '.join(tlds))
    print("Performing a whois lookup on", len(tlds), 'TLDs...')
    whois_data_registered = []
    whois_data_unregistered = []
    whois_data_errors = []
    for tld in tlds:
        while True: # this is to retry within the try/except.
            try:
                print ('whois', tld, '...')
                whois_res = whois.whois(tld) # this returns a <class 'whois.parser.WhoisCom'>
                res = {}
                res['domain_name'] = whois_res.domain_name
                res['org'] = whois_res.org
                res['expiration_date'] = whois_res.expiration_date
                res['registrar'] = whois_res.registrar
                # sometimes python-whois doesn't parse the expiration date correctly
                if (whois_res.expiration_date == None):
                    res['error_type'] = "Parsing error"
                    res['error_details'] = "Parsing error"
                    whois_data_errors.append(res)
                else:
                    whois_data_registered.append(res)
                #print ('Added to registered: ', res['domain_name'], "\t", res['org'], "\t", res['expiration_date'], "\t", res['registrar'])
            # this means the domain is not registered
            #  we just get this error with no other information like registrar or recent expiry date.
            #  add some info to the dict in case we want to check later
            except (whois.parser.PywhoisError):
                res = {}
                res['domain_name'] = tld
                res['org'] = None
                res['expiration_date'] = None
                res['registrar'] = None
                whois_data_unregistered.append(res)
                #print ('*Added to unregistered: ', res['domain_name'], "\t", res['org'], "\t", res['expiration_date'], "\t", res['registrar'])
            except ConnectionResetError:
                #print ("\tConnectionResetError")
                time.sleep(sleep_time)
                continue # retry this one
            # catch any other errors here so we can print them out.
            except Exception as ex:
                res = {}
                res['domain_name'] = tld
                res['error_type'] = type(ex).__name__
                res['error_details'] = ex.args
                whois_data_errors.append(res)
            break #normal case, everything works
    data = {}
    data['registered'] = whois_data_registered
    data['unregistered'] = whois_data_unregistered
    data['errors'] = whois_data_errors
    return data

# python-whois returns either a single item or a list/tupples.
# this returns the first item of a list or tuple.
# if it's passed a single item, we reutrn that item.
def get_first_item(data):
    if (isinstance(data,list)):
        return data[0]
    else:
        return data

# prints the two tables
# takes a dict containing the registered and unregistered list of dicts.
def print_tables(whois_data):
    registered = whois_data['registered']
    unregistered = whois_data['unregistered']
    errors = whois_data['errors']
    t = PrettyTable()
    t.field_names = ['TLD', 'Org.', 'Exp. date', 'Registrar']
    for tld in registered:
        expiration_date = get_first_item(tld['expiration_date'])
        domain_name = get_first_item(tld['domain_name'])
        org = get_first_item(tld['org'])
        registrar = get_first_item(tld['registrar'])
        t.add_row([domain_name, org, expiration_date, registrar])
    t.align = 'l'
    t.sortby = 'Exp. date'
    print ("Registered TLDs:")
    #print (registered)
    print (t)
    print ("UNREGISTERED TLDs:")
    for tld in unregistered:
        print("*", tld['domain_name'])
    print ("ERRORS:")
    for tld in errors:
        print (tld['domain_name'], "\t\t\t", tld['error_type'], '\t', tld['error_details'])



def main():
    infile = argv[1]
    possible_tlds = runregex(infile)
    tlds = validate_tlds(possible_tlds)
    whois_data = whois_lookup(list(set(tlds)))
    print_tables(whois_data)
     # list(set()) removes dupes

if __name__ == "__main__":
    main()
